extern crate reqwest;
extern crate select;
#[macro_use] extern crate prettytable;

use select::document::Document;
use select::predicate::{Class, Name, Predicate};
use prettytable::Table;

// execute get_stories and pass in URL of interest
fn main() {
    get_stories("https://slashdot.org");
}

fn get_stories(url: &str) {

    let resp = reqwest::get(url).unwrap();
    assert!(resp.status().is_success());

    let document = Document::from_read(resp).unwrap();
    
    // begin an empty table for populating
    let mut table = Table::new();

    // iterate through 'story' nodes in document
    for node in document.find(Class("story")) {
        
        // get story title
        let story = node.find(Class("story-title").descendant(Name("a")))
            .next()
            .unwrap()
            .text();

        //println!("\n | {} \n", story);
        
        // get url of story
        let url = node.find(Class("story-title").descendant(Name("a"))).next().unwrap();
        // put formatted url into furl variable
        let furl = format!("Slashlink:   https:{}",url.attr("href").unwrap());
        
        //println!("Slashlink:   https:{}",url.attr("href").unwrap());
        
        // get url of source link
        let srclnk = node.find(Class("story-sourcelnk")).next().unwrap();
        // put formatted url into fsrclnk variable
        let fsrclnk = format!("Source link: {}",srclnk.attr("href").unwrap());
        
        // println!("Source link: {}",srclnk.attr("href").unwrap());
        
        // append to table
        table.add_row(row![FdBgbl->story]);
        table.add_row(row![Fg->furl]);
        table.add_row(row![Fg->fsrclnk]);
    }
    // print table to stdout
    table.printstd();
}
