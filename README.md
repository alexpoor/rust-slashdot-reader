# Slashdot reader

A very basic news reader app which scrapes content from the Slashdot home page, and presents in a prettytable table to stdout.

## Usage

Clone the repo, or simply copy Cargo.toml and src/main.rs to a new Cargo project.
Run `cargo build` to download and install all dependencies.
Run `cargo run` to start the app.

## Useful information

This is app is for my own education and learning only.

A 'get_stories' function is executed in main, with the URL as an argument. Theoretically you can use any URL here, but unfortunately the rest of the get_stories function goes to look for specific HTML nodes in order to select content. These nodes are dependent on the way Slashdot code is built.

In any case, to tweak it for a different news site of your choice, just:
- Inspect the source of the page to understand the node of each story or article
- This node should be set at line 25 for the function to iterate over
- After that, it's simply a case of finding the other nodes you are interested in and passing those to variables
- Finally, depending on what you are selecting, you may need to add rows to the table at line 50.

## Note
There is no useful information (no information at all, actually) on select-rs at crates.io.
However there is great example documentation on the github page [here](https://github.com/utkarshkukreti/select.rs), which uses an example from Stack Overflow which is pretty cool.
